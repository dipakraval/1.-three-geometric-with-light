import './style.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader'
import * as dat from 'dat.gui'


const canvas = document.querySelector(".webgl");

/**
 * Texture Loader
 */

const GroundTexture = new THREE.TextureLoader().load( '/textures/12719.jpg' );


/**
 * Scene
 */
const scene = new THREE.Scene();
scene.background = new THREE.Color(0x000000);


/**
 * Geometry
 */
// const material = new THREE.MeshBasicMaterial();
// material.color = new THREE.Color(0x00ffe0);
// // material.map = GroundTexture
// material.side = THREE.DoubleSide;

// const material = new THREE.MeshNormalMaterial();
// material.side = THREE.DoubleSide;
// material.flatShading = true;

// const material = new THREE.MeshMatcapMaterial();
// material.side = THREE.DoubleSide;
// material.matcap = GroundTexture

// const material = new THREE.MeshLambertMaterial();
// material.side = THREE.DoubleSide;

const material = new THREE.MeshPhongMaterial();
material.shininess = 100;
material.specular = new THREE.Color(0xff0000);
material.side = THREE.DoubleSide;

const sphere = new THREE.Mesh(
	new THREE.SphereBufferGeometry(0.5,32,16),
	material
);
sphere.position.x = -2;
scene.add(sphere);

const plane = new THREE.Mesh(
	new THREE.PlaneBufferGeometry(1,1),
	material
);
scene.add(plane);

const torus = new THREE.Mesh(
	new THREE.TorusBufferGeometry(0.3,0.2,16,32),
	material
);
torus.position.x = 2
scene.add(torus);


/**
 * Text Geometry
 */
const fontLoader = new THREE.FontLoader();
fontLoader.load("fonts/helvetiker_regular.typeface.json",
	function(font){
		const text = new THREE.TextGeometry(
			"Hello Dipak",
			{
				font: font,
				size: 80,
				height: 5,
				curveSegments: 12,
				bevelEnabled: true,
				bevelThickness: 10,
				bevelSize: 8,
				bevelOffset: 0,
				bevelSegments: 5
			}
		);

		// const name = new.Mesh(
		// 	new THREE.MeshPhongMaterial
		// );

		// const name = new THREE.MeshPhongMaterial(
		// 	{
		// 		color:0xff0000
		// 	}
		// );
	}
);




/**
 * Camera
 */
const size = {
    width: window.innerWidth,
    height: window.innerHeight
}
const camera = new THREE.PerspectiveCamera(75,size.width / size.height,1,1000);
camera.position.z = 4;
scene.add( camera );


/**
 * Light
 */
const light = new THREE.AmbientLight( 0xffffff,0.5 );
scene.add(light);

const pointLight = new THREE.PointLight( 0x00ff00 , 0.5,100 );
scene.add(pointLight);
pointLight.position.x = 2;
pointLight.position.y = 3;
pointLight.position.z = 4;

const pointLight2 = new THREE.PointLight( 0xff0000 , 0.5,100 );
scene.add(pointLight2);
pointLight2.position.x = -2;
pointLight2.position.y = -3;
pointLight2.position.z = -4;

const pointLight3 = new THREE.PointLight( 0x0000ff , 0.5,100 );
scene.add(pointLight3);
pointLight3.position.x = -4;
pointLight3.position.y = 0;
pointLight3.position.z = 0;

const sphereSize = 1;
const pointLightHelper = new THREE.PointLightHelper(
	pointLight,sphereSize
);
scene.add(pointLightHelper);

const pointLightHelper2 = new THREE.PointLightHelper(
	pointLight2,sphereSize
);
scene.add(pointLightHelper2);

const pointLightHelper3 = new THREE.PointLightHelper(
	pointLight3,sphereSize
);
scene.add(pointLightHelper3);

/**
 * OrbitControls
 */
const control = new OrbitControls(camera, canvas);
control.enableDamping = true;


/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(size.width,size.height);


/**
 * Animation
 */
const tick = () => {

    control.update();
    sphere.rotation.y += 0.5 / Math.PI / 30;
    plane.rotation.y += 0.5 / Math.PI / 30;
    torus.rotation.y += 0.5 / Math.PI / 30;
    renderer.render(scene,camera);
    window.requestAnimationFrame(tick);

}
tick();