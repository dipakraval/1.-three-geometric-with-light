Model Information:
* title:	Lamborghini Terzo Millenio
* source:	https://sketchfab.com/3d-models/lamborghini-terzo-millenio-452e29bbaad04a73aebfbdb907988ca4
* author:	MRN 3d studio (https://sketchfab.com/MRN3D)

Model License:
* license type:	CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)
* requirements:	Author must be credited. Commercial use is allowed.

If you use this 3D model in your project be sure to copy paste this credit wherever you share it:
This work is based on "Lamborghini Terzo Millenio" (https://sketchfab.com/3d-models/lamborghini-terzo-millenio-452e29bbaad04a73aebfbdb907988ca4) by MRN 3d studio (https://sketchfab.com/MRN3D) licensed under CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)